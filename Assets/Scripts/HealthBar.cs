using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    private Transform bar;
    public Vector3 eulerAngle = new Vector3(0f, 0f, 0f);
    public Vector3 positionOffset = new Vector3(0f, 3f, 0f);

    private void Awake()
    {
        bar = transform.Find("Bar");
    }

    private void Start()
    {

    }

    public void SetSize(float sizeNormalized)
    {
        bar.localScale = new Vector3(sizeNormalized, 1f, 1f);
    }
}
