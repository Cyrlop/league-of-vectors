using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class PlayerMovement : NetworkBehaviour
{
    private CameraControl cameraControl;

    // Player
    public float speed;
    public bool isMoving;
    private Vector3 targetPos;
    const int rightClick = 1;

    // Pseudo
    // TODO
    //public RectTransform pseudoTransform;

    // Health bar
    [SerializeField] private HealthBar healthBar;

    // Map
    private GameObject mapCanvas;
    private float mapCanvasScaleRatio;


    private void Awake()
    {
        mapCanvas = GameObject.Find("MapCanvas");
        mapCanvasScaleRatio = mapCanvas.transform.localScale.x;
    }

    private void Start()
    {
        targetPos = transform.position;
        isMoving = false;
        cameraControl = GameObject.Find("MainCamera").GetComponent<CameraControl>();
    }

    void Update()
    {

        if (Input.GetMouseButton(rightClick) && this.isLocalPlayer)
        {
            SetTarggetPosition();
        }
        if (Input.GetKeyDown(KeyCode.S) && this.isLocalPlayer)
        {
            isMoving = false;
        }
        if (Input.GetKey(KeyCode.Space) && this.isLocalPlayer)
        {
            cameraControl.CenterCam(transform.position);
        }

        if (isMoving)
        {
            Move();
        }
    }

    void SetTarggetPosition()
    {
        targetPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        targetPos.z = 0;
        isMoving = true;
    }

    void Move()
    {
        // Rotate to where it's going
        // transform.LookAt(targetPos);
        transform.up = targetPos - transform.position;

        transform.position = Vector3.MoveTowards(transform.position, targetPos, speed * Time.deltaTime);
        // Move player body first to handle collisions, then parent player
        // playerBody.transform.position = Vector3.MoveTowards(playerBody.transform.position, targetPos, speed * Time.deltaTime);
        // transform.position = playerBody.transform.position;
        // playerBody.transform.position = new Vector3(0f, 0f);


        // Health bar rotation (non rotated)
        healthBar.transform.eulerAngles = healthBar.eulerAngle;
        // Health bar position
        healthBar.transform.position = transform.position + healthBar.positionOffset;

        // Move pseudo (text)
        // TODO
        // Vector3 pseudoOffset = new Vector3(0f, 4.5f, 0f);
        // pseudoTransform.anchoredPosition = (transform.position + pseudoOffset) / mapCanvasScaleRatio;

        if (transform.position == targetPos)
        {
            isMoving = false;
        }
    }
}
