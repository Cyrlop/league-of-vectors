using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class PlayerSpells : NetworkBehaviour
{

    public GameObject spellA;
    public GameObject spellB;
    public GameObject spellC;

    // Spells cooldown in seconds
    private float cooldownA = 1f;
    private float cooldownB = 1f;
    private float cooldownC = 2f;

    private float timeNextUseA = 0f;
    private float timeNextUseB = 0f;
    private float timeNextUseC = 0f;


    private void Start()
    {

    }

    void Update()
    {
        if ((Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.Q)) && this.isLocalPlayer)
        {
            if (timeNextUseA <= Time.time)
            {
                CmdFireSpellA();
                timeNextUseA = Time.time + cooldownA;
            }
        }
        if ((Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.W)) && this.isLocalPlayer)
        {
            if (timeNextUseB <= Time.time)
            {
                CmdFireSpellB();
                timeNextUseB = Time.time + cooldownB;
            }
        }
        if (Input.GetKeyDown(KeyCode.E) && this.isLocalPlayer)
        {
            if (timeNextUseC <= Time.time)
            {
                CmdFireSpellC();
                timeNextUseC = Time.time + cooldownC;
            }
        }
    }

    [Command]
    void CmdFireSpellA()
    {
        // To move the spell in the direction the playter is looking at
        Vector3 positionSpell = transform.position + 10 * transform.up;

        // To move the spell up the Z axiss so the spell is on the player
        positionSpell -= transform.forward;

        SpawnSpell(spellA, positionSpell, transform.rotation);
    }

    [Command]
    void CmdFireSpellB()
    {
        // To move the spell in the direction the playter is looking at
        Vector3 positionSpell = transform.position + 10 * transform.up;

        // To move the spell up the Z axiss so the spell is on the player
        positionSpell -= transform.forward;

        SpawnSpell(spellB, positionSpell, transform.rotation);
    }

    [Command]
    void CmdFireSpellC()
    {
        StartCoroutine(CoroutineSpellC());
    }

    IEnumerator CoroutineSpellC()
    {
        float totalBalls = 5;
        float distFirstBall = 8f;
        float distBetweenBalls = 5f;
        float secBetweenBalls = 0.12f;

        // To move the spell in the direction the playter is looking at
        Vector3 positionFirstBall = transform.position + transform.up * distFirstBall;

        // To move the spell up the Z axiss so the spell is on the player
        positionFirstBall -= transform.forward;

        GameObject ball = null;

        for (int numBall = 0; numBall <= totalBalls - 1; numBall++)
        {
            if (numBall == 0)
            {
                ball = SpawnSpell(spellC, positionFirstBall, transform.rotation);
            }
            else
            {
                yield return new WaitForSeconds(secBetweenBalls);
                Vector3 positionBall = ball.transform.position + ball.transform.up * distBetweenBalls;

                ball = SpawnSpell(spellC, positionBall, ball.transform.rotation);
            }
        }
    }

    GameObject SpawnSpell(GameObject spell, Vector3 positionSpell, Quaternion rotationSpell)
    {
        GameObject new_spell = Instantiate(spell, positionSpell, rotationSpell);
        NetworkServer.Spawn(new_spell);
        return new_spell;
    }
}


