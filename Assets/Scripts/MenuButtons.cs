using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButtons : MonoBehaviour
{
    public GameObject MenuPanel;
    public GameObject ControlsPanel;

    // Start is called before the first frame update
    void Start()
    {
        MenuPanel.SetActive(true);
        ControlsPanel.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ShowMenuPanel();
        }
    }

    public void ShowControlsPanel()
    {
        MenuPanel.SetActive(false);
        ControlsPanel.SetActive(true);
    }

    public void ShowMenuPanel()
    {
        MenuPanel.SetActive(true);
        ControlsPanel.SetActive(false);
    }

    public void CloseMenu()
    {
        MenuPanel.SetActive(false);
        ControlsPanel.SetActive(false);
    }
}
