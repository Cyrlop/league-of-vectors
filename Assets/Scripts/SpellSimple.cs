using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class SpellSimple : NetworkBehaviour
{
    [SerializeField] private float spellDuration;
    public float baseDamage;

    void Start()
    {
        Destroy(this.gameObject, spellDuration);
    }

    void Update()
    {

    }
}
