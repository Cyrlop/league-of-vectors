using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class PlayerHandler : NetworkBehaviour
{
    [SerializeField] private HealthBar healthBar;
    public float maxHealth;
    [SyncVar]
    public float curHealth;

    private void Awake()
    {

    }

    private void Start()
    {
        healthBar.SetSize(GetHealthRatio());
    }

    private void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent("SpellSimple") != null)
        {
            SpellSimple spell = other.gameObject.GetComponent<SpellSimple>();
            Debug.Log("You got hit by SpellSimple of type " + other.gameObject.name + " and looses " + spell.baseDamage + " HP");
            if (NetworkServer.active)
                RpcTakeHit(spell.baseDamage);
            else
                CmdTakeHit(spell.baseDamage);
        }
    }

    public void UpdateHealth(float amount)
    {
        if (curHealth + amount < 0)
        {
            curHealth = 0;
            healthBar.SetSize(GetHealthRatio());
            Die();
        }
        else if (curHealth + amount > maxHealth)
        {
            curHealth = maxHealth;
            healthBar.SetSize(GetHealthRatio());
        }
        else
        {
            curHealth += amount;
            healthBar.SetSize(GetHealthRatio());
        }
    }

    public float GetHealthRatio()
    {
        return curHealth / maxHealth;
    }

    // Take hit CMD and RPC
    [Command]
    public void CmdTakeHit(float damage)
    {
        RpcTakeHit(damage);
    }
    [ClientRpc]
    public void RpcTakeHit(float damage)
    {
        UpdateHealth(-damage);
        healthBar.SetSize(GetHealthRatio());
    }

    public void Die()
    {
        Debug.Log("You're dead!");
    }
}